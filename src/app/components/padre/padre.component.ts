import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {
personaPadre!:any;
figuraPadre!:any;

  constructor() { }

  ngOnInit(): void {
  }

  recibirObjeto($event:any):void{
    this.personaPadre = $event;
    console.log(this.personaPadre);
  }

  recibirObjeto2($event:any):void{
    this.figuraPadre = $event;
    console.log(this.figuraPadre);
    
  }
}
