import { Component, OnInit } from '@angular/core';
import { Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styleUrls: ['./hijo.component.css']
})
export class HijoComponent implements OnInit {

  Persona ={
    nombre: "Luis Fernando",
    apellidos: "Camacho Velasco",
    edad: " 24",
    sexo: " Masculino",
    estado: " Soltero",
    mensaje: "Objeto de hijo a padre confirmado"
  }

  Automovil = {
    marca:"Lamborghini",
    anio: 2013,
    modelo: "Gallardo",
    color: "Rojo",
    mensaje: "Objeto de hijo a padre confirmado "
  }

  @Output() paraEmitir = new EventEmitter<any>();
  @Output() alPadre = new EventEmitter<any>();
  constructor() { }

  ngOnInit(): void {
    this.paraEmitir.emit(this.Persona);
    this.alPadre.emit(this.Automovil)
  }

}
